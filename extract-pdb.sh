#!/bin/bash
# Name: extract-pdb.sh
# Author: Edson Silva
# Date: 05-21-2016
# Description: extract residues of .pdb
#	and convert to .xyz
# Example: bash extract-pdb.sh 3bwm.pdb

rsd1="SAM"		# residue 1
rsd2="DNC"		# residue 2
delim="LINK"	# delimita
bckg="41"		# background color
word="37"		# word color

######################
### Extract residues #
######################

if test $1
then
	grep "\($rsd1\|$rsd2\)" "$1" |
	sed "1,/^$delim/ d" | 
	sed '1d' > new-$1
fi

######################
### Convert to .xyz  #
######################

if test new-$1
then
	awk -F ' ' '{print $12, $7, $8, $9}' new-$1 | 
	sed 's/ /\t\t/g' > new-$1.xyz

	natoms=`wc -l new-$1.xyz > temp`
	natoms2=`awk -F ' ' '{print $1}' temp`
	sed -i "1s/^/$natoms2\n\n/g" new-$1.xyz

	rm temp
else
	echo -e "\033[$bckg;$word;1m File new-$1 not found! \003[m"
fi
