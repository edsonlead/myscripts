#!/bin/bash
# toCCF.sh
# Edson Silva
# 
# Convert file of chemical formats 
# It's necessary the OpenBabel be installed
#
# Example: bash toCCF.sh <file>

# Adicionar opção: deseja que o openbabel seja instalado? S/N

OPENBABEL="babel"
FILE=$1				# input
TYPE=""				# format input
NEWNAME=""			# output
NEWTYPE="mol2"		# format output
RUN=""				# run or install babel
PKG=""				# values: dnf (rpm - e.g.Fedora) or apt-get (dpkg - e.g. Debian)

##########################
### Convert File       ###
##########################

if test which -a $OPENBABEL
then
	if test $FILE
	then
		TYPE=`ls "$FILE" | cut -d . -f 2-6,3-6`
		NEWNAME=`ls "$FILE" | cut -d . -f 1`
		RUN=`babel -i$TYPE $FILE -o$NEWTYPE $NEWNAME.$NEWTYPE`
		echo $RUN
	fi

##########################
### Install OpenBabel? ###
##########################

else
	echo -e "\033[37;41;1m Sorry! It's necessary the OpenBabel be installed! \033[m"
	if [-f /usr/bin/dpkg]
	then
		PKG="apt-get"
	elif [-f /usr/bin/rpm]
	then
		PKG="dnf"
	fi
	
	sleep 3
	echo ""
	echo -e "\033[37;42;1m Starting install the OpenBabel \033[m"
	RUN=`sudo $PKG install openbabel`
	echo $RUN
fi
